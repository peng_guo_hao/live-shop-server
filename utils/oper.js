const assert = require('http-assert');
var mongodb = require('../database')
const jwt = require('jsonwebtoken')
const { tokenSecret } = require('../config')

//数据库操作
module.exports= {
    getUser,getShop
}
async function getUser(req){
    const token = String(req.headers.authorization || '').split(' ').pop();
    const { id } = jwt.verify(token, tokenSecret);
    let user = await mongodb.User.findById(id);
    assert(user,404,'没找到用户')
    return {
        user,id
    }
}
async function getShop(req){
    const token = String(req.headers.authorization || '').split(' ').pop();
    const { id } = jwt.verify(token, tokenSecret);
    let shop = await mongodb.Shop.findOne({'userID':id});
    assert(shop,404,'没找到商铺')
    return {
        shop,id
    }
}
