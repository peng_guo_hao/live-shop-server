const jwt = require('jsonwebtoken');
const assert = require('http-assert');
var mongodb = require('../database')
var {tokenSecret} = require('../config')
module.exports = (options)=>{
    return async(req,res,next)=>{
        const token = String(req.headers.authorization || '').split(' ').pop();
        assert(token, 401, '请提供jwt token');
        const { id } = jwt.verify(token, tokenSecret);
        assert(id, 401, '无效的jwt token');
        req.user = await mongodb.User.findById(id);
        assert(req.user, 401, "请先登陆");
        await next();
    }
}