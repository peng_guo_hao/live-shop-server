var express = require('express')
var showRoute = require('./routes/showRoute/index')
var manageRoute = require('./routes/manageRoute/index')
var liveRoute = require('./routes/liveRoute/index')
var bodyParser = require('body-parser')
var session = require('express-session')
var mongodb = require('./database')
var app = express()
require('./server/nms')
require('./server/ws')
// let expressWs = require('express-ws')(app)
// let io = require('socket.io')(server)

app.use(bodyParser.json({ limit: '100mb' }))
app.use(bodyParser.urlencoded({ limit: '100mb', extended: false }))
app.use(express.json());
app.use(require('cors')());
app.use('/showRoute/api', showRoute)
app.use('/manageRoute/api', manageRoute)
app.use('/liveRoute/api', liveRoute)
app.use((err, req, res, next) => {
    console.log(err)
    res.status(err.statusCode).send({
        message: err.message
    })
})

app.listen(3000,() => {
    console.log('http://localhost:3000')
})