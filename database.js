const mongoose = require('mongoose')
const config = require('./config')
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
mongoose.connect(`mongodb://localhost/${config.MongoDB.databaseName}`, { useNewUrlParser: true }, (err) => {
    if (err) {
        console.log(err + "数据库连接失败");
    }
    console.log("数据库连接成功");
})
const Models = {
    User: require('./models/user'),
    Good:require('./models/good'),
    Comment:require('./models/comment'),
    Category:require('./models/category'),
    Shop:require('./models/shop'),
    ShopCategory:require('./models/shopCategory'),
    Order:require('./models/order'),
    ShopInOrder:require('./models/shopInOrder'),
    GoodInOrder:require('./models/goodInOrder'),
    Address:require('./models/address'),
    Cart:require('./models/cart'),
    ChoiceShop:require('./models/choiceShop'),
    ChoiceShopGood:require('./models/choiceShopGood'),
    Live:require('./models/live')
}

module.exports = Models
