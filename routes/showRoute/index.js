var express = require('express')
var router = express.Router()
var fs = require('fs')
var axios = require('axios')
//express 5以上的版本才能用
var assert = require('http-assert')
//加密插件
const Bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const baiduClient = require('../../utils/baidu')
const aliPaySdk = require('../../utils/alipay')
const AlipayFormData = require('alipay-sdk/lib/form').default
var mongodb = require('../../database')
var { tokenSecret } = require('../../config')
const authMiddleware = require('../../middleware/auth')
const { default: mongoose } = require('mongoose')

const { getUser } = require('../../utils/oper')
const alipaySdk = require('../../utils/alipay')
const { populate } = require('../../models/user')
/**
 * 用户相关api
 * /user/xxx
 */
//上传图片

//注册
router.post('/user/register', async (req, res, next) => {
    req.body.createdAt = Date.now()
    req.body.updatedAt = Date.now()
    req.body.password = Bcrypt.hashSync(req.body.password, 10)
    let doc = await mongodb.User.findOne({ 'count': req.body.count })
    assert(!doc, 403, '该账号已存在，请重新设置')
    // assert(doc)
    mongodb.User.create(req.body, async (err, doc) => {
        // assert(err,403,'注册失败')
        if (err) {
            throw err
        }
        //给用户创建购物车
        let result = await mongodb.Cart.create({ 'user': doc._id })
        let cart = await mongodb.User.updateOne({ '_id': doc._id }, { $set: { cart: result._id } })
        res.send({ message: '注册成功' })
    })
})
//登录
router.post('/user/login', async (req, res, next) => {
    let { count, password } = req.body
    const doc = await mongodb.User.findOne({ "count": count })
    process.env.userID = doc._id
    assert(doc, 422, '账号不存在')
    const isValid = Bcrypt.compareSync(password, doc.password)
    assert(isValid, 422, '密码错误')
    const token = jwt.sign({ id: doc._id }, tokenSecret)
    res.send({ token })
})
//获取用户信息
router.get('/user/userInfo', authMiddleware(), async (req, res, next) => {
    let { user, id } = await getUser(req)
    res.send({ data: user })
})
//更新用户资料
router.post('/user/updateUserInfo', authMiddleware(), async (req, res, next) => {
    req.body.updatedAt = Date.now()
    if (req.body.isUpdatePassword) {
        req.body.password = Bcrypt.hashSync(req.body.password, 10)
    }
    let _id = mongoose.Types.ObjectId(req.body._id)
    delete req.body.twicePassword
    delete req.body.isUpdatePassword
    const doc = await mongodb.User.findByIdAndUpdate(_id, req.body)
    assert(doc, 422, '修改失败')
    res.send({ message: '修改成功' })
})
//开通商家时，判断是不是同一个人
router.post('/isSameImg', async (req, res, next) => {
    baiduClient.match([
        { "image": req.body.userImg, "image_type": "URL" },
        { "image": req.body.validImgSrc, "image_type": "URL" }
    ]).then(result => {
        res.send({ data: result })
    })
})
/**
 * 店铺相关api
 * /shop/xxx
 */
//根据店铺ID获取店铺信息
router.get('/shop/getShopByID', authMiddleware(), async (req, res, next) => {
    let shopID = req.query.shopID
    let docs = await mongodb.Shop.findById(shopID)
    res.send({ data: docs })
})
//开通商家服务
router.post('/shop/openShop', authMiddleware(), async (req, res, next) => {
    req.body.userID = process.env.userID
    let result = await mongodb.Shop.create(req.body)
    assert(result, 403, '开通失败')
    let doc = await mongodb.User.findOneAndUpdate({ '_id': mongoose.Types.ObjectId(req.body.userID) }, { 'isShopOwner': true, 'shopID': result._id })
    assert(doc, 403, '身份修改失败')
    res.send({ message: '开通成功' })
})
//获取店铺信息
router.get('/shop/getShopInfo', async (req, res, next) => {
    let shopID = req.query.shopID
    let docs = await mongodb.Shop.findById(shopID).populate({ path: 'category', populate: { path: 'good' } }).exec()
    let data = {
        shopDetail: null,
        shopCategory: []
    }
    for (let i = 0; i < docs.category.length; i++) {
        let item = { name: '', goods: [] }
        item.name = docs.category[i].name
        item.goods = docs.category[i].good
        data.shopCategory.push(item)
    }
    delete docs.category
    data.shopDetail = docs
    res.send({ data: data })
})
//根据分类获取分类下的商品
// router.get('/shop/getGoodByCategory', async (req, res, next) => {
//     let categoryID = req.query.categoryID
//     let goods = await mongodb.Good.find({ 'category': categoryID })
//     assert(goods, 402, '该分类下没有商品')
//     res.send({ data: goods })
// })
//根据商品id获取商品
router.get('/shop/getGoodByID', async (req, res, next) => {
    let goodID = req.query.goodID
    let good = await mongodb.Good.findById(goodID).populate('comment').populate({ path: 'category' })
    res.send({ data: good })
})
/**
 * 地址相关api
 * /address/xxx
 */
//添加地址
router.post('/address/addAddress', authMiddleware(), async (req, res, next) => {
    let address = req.body
    let doc = await mongodb.Address.create(address)
    assert(doc, 403, "未知错误")
    res.send({ message: '添加成功' })
})
//获取全部地址
router.get('/address/getAllAddress', authMiddleware(), async (req, res, next) => {
    let docs = await mongodb.Address.find({ 'userID': process.env.userID })
    let defaultAddressIndex = docs.findIndex(item => {
        return item.isDefault === true
    })
    let first = [docs[defaultAddressIndex]]
    docs.splice(defaultAddressIndex, 1)
    let result = first.concat(docs)
    assert(result, 403, "未知错误")
    res.send({ data: result })
})
//该用户拥有的地址数量
router.get('/address/getAddressNumber', authMiddleware(), async (req, res, next) => {
    let docs = await mongodb.Address.find({ 'userID': process.env.userID })
    assert(docs, 403, "未知错误")
    res.send({ data: docs.length })
})
//根据id删除地址
router.post('/address/deleteAddress', authMiddleware(), async (req, res, next) => {
    let docs = await mongodb.Address.deleteOne({ '_id': req.body.addressID })
    assert(docs, 403, "未知错误")
    res.send({ message: '删除成功' })
})
//修改地址
router.post('/address/updateAddress', authMiddleware(), async (req, res, next) => {
    let address = req.body
    if (address.isDefault) {
        let res1 = await mongodb.Address.updateMany({ 'userID': req.body.userID }, { isDefault: false })
    }
    let doc = await mongodb.Address.findByIdAndUpdate(req.body._id, req.body)
    assert(doc, 403, "未知错误")
    res.send({ message: '修改成功' })
})
/**
 * 购物车相关接口
 */
//向购物车添加
router.post('/cart/addGoodToCart', authMiddleware(), async (req, res, next) => {
    let item = req.body
    let user = req.body.user
    let goodID = req.body.goodID
    //查找有没有购物车
    let userCartId
    //用户是否有购物车
    let userIsHaveCart = await mongodb.User.findById(user)
    userCartId = userIsHaveCart.cart
    let good = await mongodb.Good.findById(item.goodID).populate({ path: 'category', populate: { path: 'shop' } }).exec()
    //商品所属商铺的商铺id
    let shopID = good.category.shop._id
    let cartIsHaveThisShop = await mongodb.ChoiceShop.findOne({ 'cart': userCartId, 'shopID': shopID })
    let cartShopID
    if (cartIsHaveThisShop) {
        cartShopID = cartIsHaveThisShop._id
    }
    if (!cartIsHaveThisShop) {
        let doc = await mongodb.Shop.findById(shopID)
        let thisShop = await mongodb.ChoiceShop.create({ 'cart': userCartId, 'shopID': shopID, 'shopName': doc.name })
        cartShopID = thisShop._id
        let docs = await mongodb.Cart.findByIdAndUpdate(userCartId, { $push: { 'choiceShops': cartShopID } })
    }
    let cartHaveThisGood = await mongodb.ChoiceShopGood.findOne({ 'goodID': goodID, 'choiceShop': cartShopID })
    if (!cartHaveThisGood) {
        delete item.user
        item.choiceShop = cartShopID
        let thisGood = await mongodb.ChoiceShopGood.create(item)
        let docs = await mongodb.ChoiceShop.findByIdAndUpdate(cartShopID, { $push: { 'choiceShopGoods': thisGood._id } })
        res.send({ message: '添加成功' })
    } else {
        let updateGood = await mongodb.ChoiceShopGood.findByIdAndUpdate(cartHaveThisGood._id, { 'number': item.number, 'allPrice': item.allPrice })
        res.send({ message: '添加成功' })
    }
})
//获取购物车商品
router.get('/cart/getCart', authMiddleware(), async (req, res, next) => {
    let docs = await mongodb.Cart.findOne({ user: process.env.userID }).populate({ path: 'choiceShops', populate: { path: 'choiceShopGoods' } }).exec()
    res.send({ data: docs })
})
//删除商品
router.post('/cart/deleteGoodFromCart', authMiddleware(), async (req, res, next) => {
    //这是一个购物车商品表中的商品ID
    let goodID = req.body.goodID
    let docs = await mongodb.ChoiceShopGood.findById(goodID).populate({ path: 'choiceShop', populate: { path: 'cart' } })
    mongodb.ChoiceShopGood.deleteOne({ _id: goodID }, async (err, doc) => {
        let result = await mongodb.ChoiceShop.updateOne({ _id: docs.choiceShop._id }, { $pull: { 'choiceShopGoods': goodID } })
        let findChocieShop = await mongodb.ChoiceShop.findById(docs.choiceShop._id)
        if (findChocieShop.choiceShopGoods.length < 1) {
            mongodb.ChoiceShop.findByIdAndDelete(docs.choiceShop._id, async (err, doc) => {
                let result = await mongodb.Cart.findByIdAndUpdate(docs.choiceShop.cart._id, { $pull: { 'choiceShops': findChocieShop._id } })
                res.send({ message: '删除成功' })
            })
        } else {
            res.send({ message: '删除成功' })
        }
    })

})
//删除购物车中的店铺
router.post('/cart/deleteChoiceShop', authMiddleware(), async (req, res, next) => {
    let result1 = await mongodb.ChoiceShopGood.deleteMany({ '_id': { $in: req.body.goodID } })
    let result2 = await mongodb.ChoiceShop.deleteMany({ '_id': { $in: req.body.shopID } })
    let result3 = await mongodb.Cart.updateOne({ user: process.env.userID }, { $pull: { choiceShops: { $in: req.body.shopID } } })
    res.send({ message: '删除成功' })
})
/**
 * 订单相关接口
 */
//创建订单
router.post('/order/createOrder', authMiddleware(), async (req, res, next) => {
    let data = req.body
    let choiceShops = data.cartData.choiceShops
    let orderItem = {
        address: data.address,
        phone: data.phone,
        user: data.user,
        name: data.name,
        description: data.description,
        allPrice: data.allPrice,
        status: data.status
    }
    let orderRes = await mongodb.Order.create(orderItem)
    //在user表中更新订单
    let updateUser = await mongodb.User.findByIdAndUpdate(data.user, { $push: { 'orders': orderRes._id } })
    //创建订单下的shop
    let shopData = []
    for (let i = 0; i < choiceShops.length; i++) {
        let doc = await mongodb.Shop.findById(choiceShops[i].shopID)
        let price = 0
        for (let j = 0; j < choiceShops[i].choiceShopGoods.length; j++) {
            price = Number(0) + Number(choiceShops[i].choiceShopGoods[j].allPrice)
        }
        let item = {
            shopID: choiceShops[i].shopID,
            order: orderRes._id,
            status: 0,
            shopName: doc.name,
            price: price
        }
        shopData.push(item)
    }
    let shopRes = await mongodb.ShopInOrder.insertMany(shopData)
    let shopResId = []
    shopRes.forEach(item => {
        shopResId.push(item._id)
    })
    let updateOrder = await mongodb.Order.updateOne({ '_id': orderRes._id }, { $push: { 'shopInOrder': { $each: shopResId } } })
    let goodInOrderData = []
    for (let i = 0; i < choiceShops.length; i++) {
        let shop = choiceShops[i]
        for (let j = 0; j < shop.choiceShopGoods.length; j++) {
            let good = shop.choiceShopGoods[j]
            delete good.choiceShop
            good.shopInOrder = shopRes[i]
            goodInOrderData.push(good)
        }
    }
    let goodRes = await mongodb.GoodInOrder.insertMany(goodInOrderData)
    for (let i = 0; i < shopResId.length; i++) {
        let data = goodRes.filter(item => {
            return item.shopInOrder._id == shopResId[i]
        })
        let updateShopInOrder = await mongodb.ShopInOrder.findByIdAndUpdate(shopResId[i], { $push: { 'goodsInOrder': { $each: data } } })
    }
    res.send({ data: orderRes })
})
//付款接口
router.post('/order/pcpay', authMiddleware(), async (req, res, next) => {
    let order = req.body
    //调用setMethod并传入get，会返回可以跳转到支付页面的url
    const formData = new AlipayFormData()
    formData.setMethod('get')
    // 通过 addField 增加参数
    // 在用户支付完成之后，支付宝服务器会根据传入的 notify_url，以 POST 请求的形式将支付结果作为参数通知到商户系统。
    formData.addField('returnUrl', 'http://localhost:8081/#/pay-success')//支付成功回调地址，必须为可以直接访问的地址，不能带参数
    formData.addField('bizContent', {
        outTradeNo: order._id, // 商户订单号,64个字符以内、可包含字母、数字、下划线,且不能重复
        productCode: 'FAST_INSTANT_TRADE_PAY', // 销售产品码，与支付宝签约的产品码名称,仅支持FAST_INSTANT_TRADE_PAY
        totalAmount: order.allPrice, // 订单总金额，单位为元，精确到小数点后两位
        subject: order._id, // 订单标题
        body: order.description, // 订单描述
    });
    const result = aliPaySdk.exec(  // result 为可以跳转到支付链接的 url
        'alipay.trade.page.pay', // 统一收单下单并支付页面接口
        {}, // api 请求的参数（包含“公共请求参数”和“业务参数”）
        { formData: formData },
    );
    result.then((resp) => {
        res.send({
            "success": true,
            "message": "success",
            "code": 200,
            "timestamp": (new Date()).getTime(),
            "result": resp
        })
    })
})
//通过支付宝查询订单付款没有的接口
router.post('/order/queryOrder', authMiddleware(), async (req, res, next) => {
    let out_trade_no = req.body.out_trade_no
    let trade_no = req.body.trade_no
    let formData = new AlipayFormData()
    formData.setMethod('get')
    formData.addField('bizContent', {
        out_trade_no,
        trade_no
    })
    const result = aliPaySdk.exec(
        'alipay.trade.query', {}, { formData: formData }
    )
    result.then(resData => {
        axios({
            url: resData,
            method: 'get'
        }).then(data => {
            let r = data.data.alipay_trade_query_response
            if (r.code === '10000') {
                switch (r.trade_status) {
                    case 'WAIT_BUYER_PAY':
                        //清空购物车
                        deleteCartContent(process.env.userID)
                        setTimeout(() => {
                            deleteOrder()
                        }, 1000 * 60 * 60 * 12)
                        res.send({
                            success: false,
                            code: 200,
                            message: '交易已创建，等待买家付款 '
                        })
                        break;
                    case 'TRADE_CLOSED':
                        res.send({
                            success: false,
                            code: 200,
                            message: '交易关闭，没有支付成功 '
                        })
                        break;
                    case 'TRADE_SUCCESS':
                        //清空购物车
                        deleteCartContent(process.env.userID)
                        //更新订单状态
                        updateStatus(out_trade_no, trade_no)
                        res.send({
                            success: true,
                            code: 200,
                            message: '交易完成,等待发货'
                        })
                        break;
                    case 'TRADE_FINISHED':
                        res.send({
                            success: true,
                            code: 200,
                            message: '交易完成，不可以退款 '
                        })
                        break;
                }
            }
        }).catch(err => {
            res.json({ message: '查询失败', err })
        })
    })
})
//退款接口
router.post('/order/pcRefund', authMiddleware(), async (req, res, next) => {
    let order = req.body
    let outTradeNo = order._id
    let tradeNo = order.tradeNo
    //调用setMethod并传入get，会返回可以跳转到支付页面的url
    const formData = new AlipayFormData()
    formData.setMethod('get')
    formData.addField('returnUrl', 'http://localhost:8081/#/refund-success')//支付成功回调地址，必须为可以直接访问的地址，不能带参数
    formData.addField('bizContent', {
        //refund_amount,out_request_no,trade_no
        outTradeNo, // 商户订单号,64个字符以内、可包含字母、数字、下划线,且不能重复
        tradeNo,
        outRequestNo: order._id,
        refundAmount: order.allPrice,
        refundReason: '我是因为什么原因呢'
    });

    const result = aliPaySdk.exec(  // result 为可以跳转到支付链接的 url
        'alipay.trade.refund', // 退款接口
        {}, // api 请求的参数（包含“公共请求参数”和“业务参数”）
        { formData: formData },
    );
    result.then((resp) => {
        res.send({
            "success": true,
            "message": "success",
            "code": 200,
            "timestamp": (new Date()).getTime(),
            "result": resp
        })
    })
})
//支付宝退款查询接口
router.post('/order/queryRefundOrder', authMiddleware(), async (req, res, next) => {
    let outTradeNo = req.body.out_trade_no
    let tradeNo = req.body.trade_no
    let formData = new AlipayFormData()
    formData.setMethod('get')
    formData.addField('bizContent', {
        tradeNo,
        outTradeNo,
        outRequestNo: outTradeNo
    })
    const result = aliPaySdk.exec(
        'alipay.trade.fastpay.refund.query', {}, { formData: formData }
    )
    result.then(resData => {
        axios({
            url: resData,
            method: 'get'
        }).then(data => {
            let r = data.data.alipay_trade_fastpay_refund_query_response
            if (r.code === '10000') {
                deleteRefundOrder(outTradeNo)
                res.send({ message: '退款成功' })
            }
        }).catch(err => {
            res.json({ message: '查询失败', err })
        })
    })
})
//查询状态0的订单,没有付款的订单
router.get('/order/findOrderBy0', authMiddleware(), async (req, res, next) => {
    let docs = await mongodb.User.findById(process.env.userID).populate({ path: 'orders', populate: { path: 'shopInOrder', populate: { path: 'goodsInOrder' } } }).exec()
    let orders = docs.orders
    let result = []
    for (let i = 0; i < orders.length; i++) {
        let shopInOrder = orders[i].shopInOrder
        for (let j = 0; j < shopInOrder.length; j++) {
            if (shopInOrder[j].status == 0) {
                result.push(shopInOrder[j])
            }
        }
    }
    res.send({ data: result })
})
//查询状态1的订单
router.get('/order/findOrderBy1', authMiddleware(), async (req, res, next) => {
    let docs = await mongodb.User.findById(process.env.userID).populate({ path: 'orders', populate: { path: 'shopInOrder', populate: { path: 'goodsInOrder' } } }).exec()
    let orders = docs.orders
    let result = []
    for (let i = 0; i < orders.length; i++) {
        let shopInOrder = orders[i].shopInOrder
        for (let j = 0; j < shopInOrder.length; j++) {
            if (shopInOrder[j].status == 1) {
                result.push(shopInOrder[j])
            }
        }
    }
    res.send({ data: result })
})
//查询状态2的订单
router.get('/order/findOrderBy2', authMiddleware(), async (req, res, next) => {
    let docs = await mongodb.User.findById(process.env.userID).populate({ path: 'orders', populate: { path: 'shopInOrder', populate: { path: 'goodsInOrder' } } }).exec()
    let orders = docs.orders
    let result = []
    for (let i = 0; i < orders.length; i++) {
        let shopInOrder = orders[i].shopInOrder
        for (let j = 0; j < shopInOrder.length; j++) {
            if (shopInOrder[j].status == 2) {
                result.push(shopInOrder[j])
            }
        }
    }
    res.send({ data: result })
})
//查询状态3的订单
router.get('/order/findOrderBy3', authMiddleware(), async (req, res, next) => {
    let docs = await mongodb.User.findById(process.env.userID).populate({ path: 'orders', populate: { path: 'shopInOrder', populate: { path: 'goodsInOrder' } } }).exec()
    let orders = docs.orders
    let result = []
    for (let i = 0; i < orders.length; i++) {
        let shopInOrder = orders[i].shopInOrder
        for (let j = 0; j < shopInOrder.length; j++) {
            if (shopInOrder[j].status == 3) {
                result.push(shopInOrder[j])
            }
        }
    }
    res.send({ data: result })
})
//查询全部的订单
router.get('/order/findOrderBy5', authMiddleware(), async (req, res, next) => {
    let docs = await mongodb.User.findById(process.env.userID).populate({ path: 'orders', populate: { path: 'shopInOrder', populate: { path: 'goodsInOrder' } } }).exec()
    let orders = docs.orders
    let result = []
    for (let i = 0; i < orders.length; i++) {
        let shopInOrder = orders[i].shopInOrder
        for (let j = 0; j < shopInOrder.length; j++) {
            result.push(shopInOrder[j])
        }
    }
    res.send({ data: result })
})
//确认收货的接口
router.post('/order/confirmOrder', authMiddleware(), async (req, res, next) => {
    let _id = req.body.shopInOrderID
    let confirm = await mongodb.ShopInOrder.findByIdAndUpdate(_id, { status: '3' })
    res.send({ message: '成功' })
})
//根据shopInOrder查到属于哪个订单
router.post('/order/whichOrder', authMiddleware(), async (req, res, next) => {
    let _id = req.body.shopInOrderID
    let order = await mongodb.Order.findOne({ 'shopInOrder': _id })
    res.send({ data: order })
})

//关注别人
router.post('/followOther', authMiddleware(), async (req, res, next) => {
    let _id = req.body.otherID
    await mongodb.User.findByIdAndUpdate(process.env.userID, { $push: { follow: _id } })
    await mongodb.User.findByIdAndUpdate(_id, { $push: { fans: process.env.userID } })
    res.send({ message: '关注成功' })
})
//取消关注别人
router.post('/unfollowOther', authMiddleware(), async (req, res, next) => {
    let _id = req.body.otherID
    await mongodb.User.findByIdAndUpdate(process.env.userID, { $pull: { follow: _id } })
    await mongodb.User.findByIdAndUpdate(_id, { $pull: { fans: process.env.userID } })
    res.send({ message: '取关成功' })
})
//获取我的关注
router.get('/getFollow', authMiddleware(), async (req, res, next) => {
    let result = await mongodb.User.findById(process.env.userID).populate('follow').exec()
    res.send({ data: result.follow })
})
//获取我的粉丝
router.get('/getFans', authMiddleware(), async (req, res, next) => {
    let result = await mongodb.User.findById(process.env.userID).populate('fans').exec()
    res.send({ data: result.fans })
})
//收藏商品
router.post('/starGood', authMiddleware(), async (req, res, next) => {
    let _id = req.body.goodID
    await mongodb.User.findByIdAndUpdate(process.env.userID, { $push: { stars: _id } })
    res.send({ message: '收藏成功' })
})
//取消收藏
router.post('/unStarGood', authMiddleware(), async (req, res, next) => {
    let _id = req.body.goodID
    await mongodb.User.findByIdAndUpdate(process.env.userID, { $pull: { stars: _id } })
    res.send({ message: '取消收藏成功' })
})
//是否在收藏夹中
router.post('/isStar', authMiddleware(), async (req, res, next) => {
    let _id = req.body.goodID
    let doc = await mongodb.User.findOne({ _id: process.env.userID, stars: _id })
    if (doc) {
        res.send({ data: true })
    } else {
        res.send({ data: false })
    }
})
//获取收藏夹商品
router.get('/getStar', authMiddleware(), async (req, res, next) => {
    let docs = await mongodb.User.findById(process.env.userID).populate('stars')
    res.send({ data: docs.stars })
})
//进行商品评价
router.post('/commentGood', authMiddleware(), async (req, res, next) => {
    let data = req.body
    let goodIDArray = []
    for (let i = 0; i < data.shop.goodsInOrder.length; i++) {
        let good = data.shop.goodsInOrder[i].good
        goodIDArray.push(mongoose.Types.ObjectId(good._id))
    }
    let comment = {
        from: data.user,
        good: goodIDArray,
        tranlevel: data.tranlevel,
        goodlevel: data.goodlevel,
        content: data.content
    }
    let result = await mongodb.Comment.create(comment)
    // await mongodb.Good.findByIdAndUpdate(comment.good,{$push:{'comment':result._id}})
    await mongodb.Good.updateMany({ "_id": { $in: goodIDArray } }, { $push: { 'comment': result._id } })
    await mongodb.ShopInOrder.findByIdAndUpdate(data.shop._id, { status: 4 })
    res.send({ data: '评论成功' })
})
//根据商品id获取商品评价
router.get('/getGoodComment', authMiddleware(), async (req, res, next) => {
    let goodID = req.query.goodID
    let docs = await mongodb.Comment.find({ good: goodID })
    res.send({ data: docs })
})
//获取商品列表
router.get('/getGoods', async (req, res, next) => {
    let docs = await mongodb.Good.find()
    docs.sort((a, b) => {
        return b.comment.length - a.comment.length
    })
    let result = []
    if (docs.length > 10) {
        for (let i = 0; i < 10; i++) {
            result.push(docs[i])
        }
    } else {
        result = docs
    }
    res.send({ data: result })
})
//获取商品列表
router.get('/getNewGoods', async (req, res, next) => {
    let docs = await mongodb.Good.find({}).sort({ createdAt: -1 })
    let result = []
    if (docs.length > 10) {
        for (let i = 0; i < 10; i++) {
            result.push(docs[i])
        }
    } else {
        result = docs
    }
    res.send({ data: result })
})
//搜索功能
router.get('/searchBy', async (req, res, next) => {
    let by = req.query.by
    let content = req.query.content
    console.log(by,content)
    let result
    var reg = new RegExp(content, 'i')
    if (by == 'good') {
        let docs = await mongodb.Good.find({
            $or: [
                { 'goodName': { $regex: reg } },
            ]
        }).populate('category').exec()
        result = docs
    }
    if (by == 'live') {
        let docs = await mongodb.Live.find({
            $or: [
                { title: { $regex: reg } },
                { category: { $regex: reg } }
            ]
        })
        result = docs
    }
    console.log(result)
    res.send({data:result})
})
//删除购物车内容
async function deleteCartContent(userID) {
    let res1 = await mongodb.Cart.findOneAndUpdate({ user: userID }, { 'choiceShops': [] })
    let cartID = res1._id
    let res2 = await mongodb.ChoiceShop.find({ cart: cartID })
    let res3 = await mongodb.ChoiceShop.deleteMany({ cart: cartID })
    let cartIDArray = []
    for (let i = 0; i < res2.length; i++) {
        cartIDArray.push(res2[i]._id)
    }
    let res = await mongodb.ChoiceShopGood.deleteMany({ cart: { $in: cartIDArray } })
}
//12小时后销毁未付款订单
async function deleteOrder(orderID) {
    let res1 = await mongodb.Order.findByIdAndDelete(orderID)
    //找到状态为0,没有付款的内容
    let res2 = await mongodb.ShopInOrder.find({ order: orderID, status: 0 })
    let res3 = await mongodb.ShopInOrder.deleteMany({ order: orderID, status: 0 })
    let shopIdArry = []
    for (let i = 0; i < res2.length; i++) {
        shopIdArry.push(res2[i]._id)
    }
    let res = await mongodb.GoodInOrder.deleteMany({ shopInOrder: { $in: shopIdArry } })
}
//销毁退款订单
async function deleteRefundOrder(orderID) {
    let res1 = await mongodb.Order.findByIdAndDelete(orderID)
    //找到状态为0,没有付款的内容
    let res2 = await mongodb.ShopInOrder.find({ order: orderID })
    let res3 = await mongodb.ShopInOrder.deleteMany({ order: orderID })
    let shopIdArry = []
    for (let i = 0; i < res2.length; i++) {
        shopIdArry.push(res2[i]._id)
    }
    let res = await mongodb.GoodInOrder.deleteMany({ shopInOrder: { $in: shopIdArry } })
}
async function updateStatus(orderID, trade_no) {
    let res1 = await mongodb.ShopInOrder.updateMany({ order: orderID }, { status: 1 })
    await mongodb.Order.findByIdAndUpdate(orderID, { tradeNo: trade_no })
}
module.exports = router