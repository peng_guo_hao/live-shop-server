var express = require('express')
var router = express.Router()
//express 5以上的版本才能用
var assert = require('http-assert')
var mongodb = require('../../database')
const path = require('path')
const authMiddleware = require('../../middleware/auth')

//创建直播间
router.get('/createLiveRoom', authMiddleware(), async (req, res, next) => {
    let user = await mongodb.User.findById(process.env.userID)
    assert(user, 402, '没有用户信息')
    let live = await mongodb.Live.create({ user: user._id, title: `${user.nickname}的直播间`, coverImgUrl: '', playUrl: `/live/${user._id}.flv` ,category:'乡村文化' })
    await mongodb.User.findByIdAndUpdate(process.env.userID, { live: live._id })
    assert(live, 402, '开通失败')
    res.send({ message: '开通直播成功' })
})
//获取直播间信息
router.get('/getLiveDetail', authMiddleware(), async (req, res, next) => {
    let userID = req.query.userID
    let live = await mongodb.Live.findOne({ user: userID })
    res.send({ data: live })
})
//获取直播间房主信息
router.get('/getLiveMaster', async (req, res, next) => {
    let userID = req.query.userID
    let user = await mongodb.User.findById(userID)
    res.send({ data: user })
})
//开启直播
router.post('/openLive', authMiddleware(), async (req, res, next) => {
    let live = await mongodb.Live.findByIdAndUpdate(req.body.liveID, { title: req.body.title, coverImgUrl: req.body.coverImgUrl, category: req.body.category, onLive: true })
    res.send({ message: '直播开启成功' })
})
//关闭直播
router.post('/closeLive', authMiddleware(), async (req, res, next) => {
    let live = await mongodb.Live.findByIdAndUpdate(req.body.liveID, { onLive: false })
    res.send({ message: '直播关闭成功' })
})
//获取正在直播的房间
router.get('/getLives', async (req, res, next) => {
    let category = req.query.category
    let lives = await mongodb.Live.find({ category: category, onLive: true })
    // let lives = await mongodb.Live.find({ category: category })
    res.send({ data: lives })
})
module.exports = router