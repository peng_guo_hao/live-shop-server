var express = require('express')
var router = express.Router()
//express 5以上的版本才能用
var assert = require('http-assert')
var mongodb = require('../../database')
const formidable = require('formidable');
const path = require('path')
const authMiddleware = require('../../middleware/auth')
const { getShop } = require('../../utils/oper')
const ossClient = require('../../utils/oss')
const { default: mongoose } = require('mongoose')

router.post('/addImgFile', function (req, res, next) {
    let form = new formidable.IncomingForm()
    // 设置编码
    form.encoding = 'utf-8';
    // 保留后缀名
    form.keepExtensions = true;
    form.parse(req, async (err, fields, files) => {
        let ossPath = fields.flag ? fields.flag : ''
        assert(!err, 403, '未知错误')
        let localpath = files.file.filepath
        const result = await ossClient.put(`${ossPath}/` + files.file.originalFilename, path.normalize(localpath))
        res.send({ data: { url: result.url, name: files.file.originalFilename }, message: '图片上传成功' })
    })
});

//删除分类,并且删除分类下的商品
router.post('/category/deleteCategory', authMiddleware(), async (req, res, next) => {
    //删除shop表中的分类
    let result1 = await mongodb.Shop.findOneAndUpdate({ 'userID': process.env.userID }, { $pull: { category: req.body.categoryID } })
    //删除分类表中的分类
    let result2 = await mongodb.Category.findByIdAndDelete(req.body.categoryID)
    //删除分类下的商品
    // let result1 = await mongodb.Good.deleteMany({ 'userID': process.env.userID, 'category': req.body.name })
    res.send({ message: '删除成功' })
})
// 修改分类 {originName:'原分类名字',category:'新的分类对象'}
router.post('/category/updateCategory', authMiddleware(), async (req, res, next) => {
    assert(process.env.userID, 402, '没有用户ID')
    let first = await mongodb.Category.updateMany({ 'name': req.body.originName, 'userID': process.env.userID, 'belonged': undefined }, { 'name': req.body.category.name, 'description': req.body.category.description })
    let second = await mongodb.Good.updateMany({ 'category': req.body.originName, 'userID': process.env.userID }, { 'category': req.body.category.name })
    res.send({ message: '修改成功' })
})
//添加分类
router.post('/category/addCategory', authMiddleware(), async (req, res, next) => {
    assert(process.env.userID, 402, '没有用户ID')
    let category = await mongodb.Category.findOne({ 'name': req.body.name, 'userID': process.env.userID })
    assert(!category, 403, '该分类已存在，请重新设置')
    req.body.userID = process.env.userID
    let doc = await mongodb.Category.create(req.body)
    let result = await mongodb.Shop.findOneAndUpdate({ 'userID': process.env.userID }, { $push: { 'category': doc._id } })
    res.send({ message: '添加成功' })
})
//获取分类
router.get('/category/getCategory', authMiddleware(), async (req, res, next) => {
    assert(process.env.userID, 402, '没有用户ID')
    let docs = await mongodb.Category.find({ userID: process.env.userID })
    res.send({ data: docs })
})
//添加商品
router.post('/good/addGood', authMiddleware(), async (req, res, next) => {
    assert(process.env.userID, 402, '没有用户ID')
    req.body.userID = process.env.userID
    let doc = await mongodb.Good.create(req.body)
    let doc2 = await mongodb.Category.findByIdAndUpdate(req.body.category, { $push: { good: doc._id } })
    assert(doc, 403, '未知错误')
    res.send({ message: '添加成功' })
})
//删除商品,需要商品id，和用户id   {_id}
router.post('/good/deleteGood', authMiddleware(), async (req, res, next) => {
    assert(process.env.userID, 402, '没有用户ID')
    let doc1 = await mongodb.Good.deleteMany({ '_id': { $in: req.body.goodID } })
    let doc2 = await mongodb.Category.updateMany({ '_id': { $in: req.body.categoryID } }, { $pull: { good: { $in: req.body.goodID } } })
    // for(let i=0;i<req.body.length;i++){
    //     mongodb.Good.deleteOne({'userID':userID,'_id':mongoose.Types.ObjectId(req.body._id)})
    // }
    // let doc = await mongodb.Good.deleteOne({'userID':userID,'_id':mongoose.Types.ObjectId(req.body._id)})
    // assert(doc, 403, '未知错误')
    res.send({ message: '删除成功' })
})
//修改商品,需要商品id，和用户id  userID:'',good:good}
router.post('/good/updateGood', authMiddleware(), async (req, res, next) => {
    assert(process.env.userID, 402, '没有用户ID')
    //第一个参数是查找的条件，第二个是更新的内容    
    let result = await mongodb.Good.findByIdAndUpdate({ '_id': mongoose.Types.ObjectId(req.body._id), 'userID': process.env.userID }, req.body)
    res.send({ data: result, message: '更新成功' })
})
//获取所有商品
router.get('/good/getGoodList', authMiddleware(), async (req, res, next) => {
    assert(process.env.userID, 402, '没有用户ID')
    let result = await mongodb.Good.find({ 'userID': process.env.userID })
    res.send({ data: result, message: '获取商品成功' })
})
//创建直播中的商品集
router.post('/good/createLiveShop', authMiddleware(), async (req, res, next) => {
    let goodIDArray = req.body
    await mongodb.Good.updateMany({ '_id': { $in: goodIDArray } }, { isInLiveShop: true })
    let docs = await mongodb.Shop.findByIdAndUpdate(process.env.shopID, { $push: { 'liveShopContent': { $each: goodIDArray } } })
    res.send({ message: '成功' })
})
//获取直播中的商品集
router.get('/live/getLiveGood', authMiddleware(), async (req, res, next) => {
    let shopID = req.query.shopID
    let docs = await mongodb.Shop.findById(shopID).populate('liveShopContent')
    res.send({ data: docs.liveShopContent })
})
//删除直播中的某个商品
router.post('/live/deleteLiveGood', authMiddleware(), async (req, res, next) => {
    let goodIDArray = req.body
    let docs = await mongodb.Shop.findByIdAndUpdate(process.env.shopID, { $pull: { 'liveShopContent': { $in: goodIDArray } } })
    res.send({ message: '成功' })
})
//获取shop信息
router.get('/shopInfo', authMiddleware(), async (req, res, next) => {
    assert(process.env.userID, 402, '没有用户ID')
    let { shop, id } = await getShop(req)
    process.env.shopID = shop._id
    res.send({ data: shop })
})
//更新shop信息
router.post('/updateShop', authMiddleware(), async (req, res, next) => {
    assert(process.env.userID, 402, '没有用户ID')
    let doc = await mongodb.Shop.updateOne({ 'userID': process.env.userID }, { 'name': req.body.name, 'IdNo': req.body.IdNo, 'telephone': req.body.telephone, 'shopDescription': req.body.shopDescription, 'sendAddress': req.body.sendAddress })
    assert(doc, 403, '出错了')
    res.send({ message: '修改成功' })
})

//订单相关接口
//获取属于店铺的所有订单
router.get('/order/getOrderList', authMiddleware(), async (req, res, next) => {
    let shopID = process.env.shopID
    let docs = await mongodb.ShopInOrder.find({ 'shopID': shopID }).populate({ path: 'goodsInOrder' })
    let result = docs.filter(item => {
        return item.status != 0
    })
    res.send({ data: result })
})
router.post('/order/getOrderGoods', authMiddleware(), async (req, res, next) => {
    let _id = req.body.shopInOrderID
    // 商品信息
    let goods = await mongodb.GoodInOrder.find({ 'shopInOrder': _id })
    //订单表找收货人信息
    let receivePerson = await mongodb.Order.findOne({ 'shopInOrder': _id })
    let order = await mongodb.ShopInOrder.findById(_id)
    res.send({ data: { goods, receivePerson, order } })
})
router.post('/order/getRecivePerson', authMiddleware(), async (req, res, next) => {
    let _id = req.body.shopInOrderID
    //订单表找收货人信息
    let receivePerson = await mongodb.Order.findOne({ 'shopInOrder': _id })
    res.send({ data: { receivePerson } })
})
//发货接口
router.post('/order/sendGoods', authMiddleware(), async (req, res, next) => {
    let _id = req.body.shopInOrderID
    let order = await mongodb.ShopInOrder.findByIdAndUpdate(_id, { status: '2' })
    res.send({ message: '发货成功' })
})
//添加封面
router.post('/addCover', authMiddleware(), async (req, res, next) => {
    let imgSrc=req.body.imgSrc
    await mongodb.Shop.findByIdAndUpdate(process.env.shopID,{$push:{'coverImgs':imgSrc}})
    res.send({message:'修改成功'})
})
//获取封面
router.get('/getCover', authMiddleware(), async (req, res, next) => {
    let docs=await mongodb.Shop.findById(process.env.shopID)
    res.send({data:docs.coverImgs})
})
//删除封面
router.post('/deleteCover', authMiddleware(), async (req, res, next) => {
    let imgSrc=req.body.imgSrc
    await mongodb.Shop.findByIdAndUpdate(process.env.shopID,{$pull:{'coverImgs':imgSrc}})
    res.send({message:'删除成功'})
})
module.exports = router