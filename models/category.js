const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Category=new Schema({
    name:{
        type:String,
        required:true
    },
    description:{
        type:String,
    },
    //分类下的商品
    good:[{
        type: Schema.Types.ObjectId,
        ref:'Good'
    }],
    //所属shop
    shop:{
        type: Schema.Types.ObjectId,
        ref:'Shop'
    },
    userID:{
        type:String
    },
    createAt:{
        type:Date,
        default:Date.now
    },
    updateAt:{
        type:Date,
        default:Date.now
    }
})

module.exports=mongoose.model('Category', Category)