const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Order = new Schema({
    address:{
        type:Object
    },
    phone:{
        type:String
    },
    //接收人姓名
    name:{
        type:String
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:'User'
    },
    shopInOrder:[{
        type:Schema.Types.ObjectId,
        ref:'ShopInOrder'
    }],
    description:{
        type:String
    },
    allPrice:{
        type:String
    },
    createAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    },
    //支付宝交易号
    tradeNo:{
        type:String
    },

})

module.exports=mongoose.model('Order', Order)