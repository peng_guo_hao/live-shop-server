const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Good = new Schema({
    goodID: {
        type: Number
    },
    goodName: {
        type: String
    },
    goodImgs: {
        type: Array,
        default:[]
    },
    goodPrice: {
        type: Object
    },
    comment: [{
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    //净重
    weight:{
        type:String,
        default:''
    },
    //单位
    unit:{
        type:String,
        default:'kg'
    },
    //库存
    inventory:{
        type:Number
    },
    category:{
        type:Schema.Types.ObjectId,
        ref:'Category'
    },
    userID:{
        type:String
    },
    createAt:{
        type:Date,
        default:Date.now
    },
    isInLiveShop:{
        type:Boolean,
        default:false
    },
    updateAt:{
        type:Date,
        default:Date.nowku
    }
})

module.exports=mongoose.model('Good', Good)