const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const GoodInOrder = new Schema({
    goodID:{
        type:String
    },
    good:{
        type:Object
    },
    number:{
        type:Number
    },
    allPrice:{
        type:String
    },
    shopInOrder:{
        type:Schema.Types.ObjectId,
        ref:'ShopInOrder'
    },
    createAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    }
})

module.exports=mongoose.model('GoodInOrder', GoodInOrder)