const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Live = new Schema({
    user:{
        type:Schema.Types.ObjectId,
        ref:'User'
    },
    title:{
        type:String
    },
    category:{
        type:String,
        default:'乡村文化',
    },
    //默认封面
    coverImgUrl:{type:String,default:''},
    //拉流地址
    playUrl:{
        type:String
    },
    onLive:{
        type:Boolean,
        default:false
    }
})

module.exports=mongoose.model('Live', Live)