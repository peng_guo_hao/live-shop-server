//购物车包含的店铺
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const ChoiceShop = new Schema({
    cart:{
        type:Schema.Types.ObjectId,
        ref:'Cart'
    },
    shopName:{
        type:String
    },
    //卖家店铺
    shopID:{
        type:String
    },
    choiceShopGoods:[{
        type:Schema.Types.ObjectId,
        ref:'ChoiceShopGood'
    }],
})

module.exports=mongoose.model('ChoiceShop', ChoiceShop)