//购物车包含的店铺
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const ChoiceShopGood = new Schema({
    goodID:{
        type:String
    },
    good:{
        type:Object
    },
    number:{
        type:Number
    },
    allPrice:{
        type:String
    },
    choiceShop:{
        type:Schema.Types.ObjectId,
        ref:'ChoiceShop'
    }
})

module.exports=mongoose.model('ChoiceShopGood', ChoiceShopGood)