const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Address = new Schema({
    userID:{
        type:String
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:'User'
    },
    //收货人姓名
    name:{
        type:String
    },
    //收货人电话
    phone:{
        type:String
    },
    isDefault:{
        type:Boolean,
        default:false
    },
    province:{
        type:String
    },
    city:{
        type:String
    },
    area:{
        type:String
    },
    //详细地址
    detailAddress:{
        type:String
    },
    createAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    }
})

module.exports=mongoose.model('Address', Address)