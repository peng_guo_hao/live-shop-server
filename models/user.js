const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const User = new Schema({
    nickname: {
        type: String,
        default: function () {
            return Math.random() * 10 + '随机的昵称'
        }
    },
    count: {
        type: String,
        required: true

    },
    password: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        default: 18,
    },
    //0女生，1男生
    sex: {
        type: Number,
        default: 0,
    },
    phone: {
        type: String
    },
    address: {
        type: Object
    },
    email: {
        type: String
    },
    follow: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    fans: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    isShopOwner: {
        type: Boolean,
        default: false
    },
    shopID: {
        type: String
    },
    userImg: {
        type: String,
        default:'http://penggh.oss-cn-beijing.aliyuncs.com/userImg/QQ%E5%9B%BE%E7%89%8720210706175940.jpg'
    },
    orders: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Order'
        }
    ],
    receiveAddress: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Address'
        }
    ],
    cart:{
        type: Schema.Types.ObjectId,
        ref: 'Cart'
    },
    live:{
        type: Schema.Types.ObjectId,
        ref: 'Live'
    },
    //收藏的商品
    stars:[{
        type: Schema.Types.ObjectId,
        ref: 'Good'
    }],
    createAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('User', User)