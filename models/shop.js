const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Shop = new Schema({
    userID: {
        type: String
    },
    name: {
        type: String
    },
    //身份证号
    IdNo: {
        type: String
    },
    userImg: {
        type: String
    },
    telephone: {
        type: String
    },
    shopDescription: {
        type: String,
        maxlength: 50
    },
    //发货地址
    sendAddress: {
        type: Object
    },
    // liveID:{
    //     type:String
    // },
    fans: {
        type: Number,
        default: 0
    },
    //物流评价的等级，1，2，3，4，5
    tranlevel: {
        type: Number,
        default: 5
    },
    //商品评价的等级，1，2，3，4，5
    goodlevel: {
        type: Number,
        default: 5
    },
    // //店铺所属分类
    // shopCategory:{
    //   type:mongoose.Types.ObjectId,
    //   ref:'ShopCategory'  
    // },
    //店铺拥有的分类
    category: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Category'
        }
    ],
    liveShopContent: [{
        type: Schema.Types.ObjectId,
        ref: 'Good'
    }],
    coverImgs:[
        {
            type:String,
            default:''
        }
    ],
    createAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Shop', Shop)