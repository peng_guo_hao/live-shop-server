const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const ShopInOrder = new Schema({
    shopID: {
        type: String
    },
    shopName: {
        type: String
    },
    order: {
        type: Schema.Types.ObjectId,
        ref: 'Order'
    },
    price: {
        type: String
    },
    goodsInOrder: [{
        type: Schema.Types.ObjectId,
        ref: 'GoodInOrder'
    }],
    //0：订单已创建，//1:已付款，待发货 //2, 已发货，待收货 // 3,确认收货。//4已经评价过了
    status: {
        type: Number,
        default: 0
    },
    createAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('ShopInOrder', ShopInOrder)