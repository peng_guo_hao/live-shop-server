const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Comment=new Schema({
    from:{
        type: Schema.Types.ObjectId,
        ref:'User',
    },
    good:[{
        type: Schema.Types.ObjectId,
        ref:'Good'
    }],
    //物流评价的等级，1，2，3，4，5
    tranlevel:{
        type:Number
    },
    //商品评价的等级，1，2，3，4，5
    goodlevel:{
        type:Number
    },
    content:{
        type:String,
        maxlength:50
    },
    createAt:{
        type:Date,
        default:Date.now
    },
    updateAt:{
        type:Date,
        default:Date.now
    }
})

module.exports=mongoose.model('Comment', Comment)