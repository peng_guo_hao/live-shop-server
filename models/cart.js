const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Cart = new Schema({
    user:{
        type:Schema.Types.ObjectId,
        ref:'User'
    },
    //购物车下的shop
    choiceShops:[{
        type:Schema.Types.ObjectId,
        ref:'ChoiceShop'
    }],
})

module.exports=mongoose.model('Cart', Cart)