
const NodeMediaServer = require('node-media-server')
const mongodb = require('../database')
require('./ws')
const config = {
    rtmp: {
        port: 1935,
        chunk_size: 60000,
        gop_cache: true,
        ping: 30,
        ping_timeout: 60
    },
    http: {
        port: 8000,
        allow_origin: '*'
    }
}
var nms = new NodeMediaServer(config)
nms.run()

nms.on('prePublish', async (id, StreamPath, args) => {
    console.log('我在这开始')
    console.log('[NodeEvent on prePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
    let session = nms.getSession(id);
    let user = StreamPath.split('/').pop()
    await mongodb.Live.findOneAndUpdate({ user }, { onLive: true })
    // session.reject();
});

nms.on('donePublish', async (id, StreamPath, args) => {
    console.log('我在这结束')
    console.log('[NodeEvent on donePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
    let user = StreamPath.split('/').pop()
    await mongodb.Live.findOneAndUpdate({ user }, { onLive: false })
});